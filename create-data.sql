drop table if exists A;
create table A(
    dimension_1 character,
    dimension_2 character,
    dimension_3 character,
    measure_1 integer);
insert into A(dimension_1,dimension_2,dimension_3,measure_1)
values
('a','I','K',1),
('a','J','L',7),
('b','I','M',2),
('c','J','N',5);

drop table if exists B;
create table B(
    dimension_1 character,
    dimension_2 character,
    measure_2 integer);

insert into B(dimension_1,dimension_2,measure_2)
values
('a','J',7),
('b','J',10),
('d','J',4);

drop table if exists MAP;
create table MAP (
    dimension_1 character,
    correct_dimension_2 character);

insert into MAP(dimension_1,correct_dimension_2)
Values
('a','W'),
('a','W'),
('b','X'),
('c','Y'),
('b','X'),
('d','Z');