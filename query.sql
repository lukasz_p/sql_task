select
    d.dimension_1,
    d.dimension_2,
    sum(measure_1) as sum_measure_1,
    sum(measure_2) as sum_measure_2
from
    (select
        a.dimension_1           as dimension_1,
        map.correct_dimension_2 as dimension_2,
        a.measure_1             as measure_1,
        0                       as measure_2

    from a
        join map
            on a.dimension_1 = map.dimension_1
    union
    select
        b.dimension_1           as dimension_1,
        map.correct_dimension_2 as dimension_2,
        0                       as measure_1,
        b.measure_2             as measure_2
    from b
        join map
            on b.dimension_1 = map.dimension_1)as d
group by 1,2
;
